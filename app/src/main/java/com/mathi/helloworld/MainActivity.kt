package com.mathi.helloworld

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    var text3: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val text = findViewById(R.id.text) as TextView
        text.setText("Hello World!")

        var text2 = findViewById(R.id.text2) as TextView
        text2.setText("Welcome to Android Development!")

        text3 = findViewById(R.id.text3) as TextView
        text3!!.setText("Welcome to Kotlin!")
    }
}
